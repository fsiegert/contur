#!/usr/bin/env python

import os
import sys

import argparse


parser = argparse.ArgumentParser(description="Extract the cross sections of all physics processes simulated with Herwig from the given directory.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("inputDir", help="Path to scan directory")
parser.add_argument("--br", "--foldBRs", help="Whether or not to fold in the SM and BSM branching ratios",
                  dest="_splitByBR", default=False, action="store_true")
parser.add_argument("--bsm_br", "--foldBSMBRs", help="Whether or not to fold in the BSM branching ratios ",
                  dest="_splitByBSMBR", default=False, action="store_true")
parser.add_argument("--sl", "--splitLeptons", help="Don't merge e, mu, tau to l",
                  dest="_splitLeptons", default=False, action="store_true")
parser.add_argument("--mb", "--mergeBosons", help="Set W, Z, H to V",
                  dest="_mergeEWBosons", default=False, action="store_true")
parser.add_argument("--sp", "--splitPartons", help="Split the incoming partons, don't just set them to pp",
                  dest="_splitIncomingPartons", default=False, action="store_true")
parser.add_argument("--sa", "--splitAntiparticles", help="Particles and antiparticles are merged by default. Add this options to split them out",
                  dest="_splitAntiParticles", default=False, action="store_true")
parser.add_argument("--sb", "--split_b", help="u, d, s, c, b are grouped into q by default. Add this options to split out the b",
                  dest="_splitBQuarks", default=False, action="store_true")
parser.add_argument("--sq", "--splitLightQuarks", help="u, d, s, c, b are grouped into q by default. Add this options to split them out",
                  dest="_splitLightQuarks", default=False, action="store_true")
parser.add_argument("-t", "--tolerance",
                  help="Minimum cross-section in fb for a process to be drawn", dest="_tolerance", default=0.01)
parser.add_argument("--ws","--website", help="Alternative format output for web-visializer", dest="_ws", default=False, action="store_true")
args = parser.parse_args()


if args._splitByBSMBR:
    args._splitByBR = True

#args._ws=1
sm_brs = {

}

herwigMEChainMap ={} # a map between the incomprehensible garble of the 
#herwig process names to something Human-readable eg: MEdubar2Y1W- d,ubar->Y1,W
# populated from the .out files "Detailed breakdown" lines

recognisedParticles = {
   "nu" : r"\nu",
   "mu" : r"\mu",
   "tau" : r"\tau",
   #"e" : "l",
   #"u" : "u",
   #"d" : "d",
   #"s" : "s",
   #"c" : "c",
   #"t" : "t",
   #"b" : "b",
   #"q" : "q",
   #"g" : "g",
   "W+" : "W",
   "W-" : "W",
   "Z0" : "Z",
   #"H" : "H",
   "gamma": " \\gamma ",  # TODO
}

#### HELPER FUNCTIONS  ####

def cleanupCommas(text):
  text=text.replace(","," ")
  while "  " in text:
    text=text.replace("  ", " ")
  return text.strip()

def makeLatexSafe(text, debug=0, isME=0):
    text = text.replace("bar", "BAR")
    #for p, lat in bsmParticles.items():
    #    text = text.replace(p, " "+lat)
    if isME:
      text = text.replace("ME", "")
    text = text.replace(",", ", ")
    text = text.replace("->", ", > ,")
    text = removeNeutrinoFlavours(text)
    for p, lat in sorted(recognisedParticles.items(), key=lambda x: len(x[0]), reverse=True):
        lat = lat.replace("tau", "TAU").replace("mu", "MU").replace(
            "nu", "NU").replace("gamma", "GAMMA")
        text = text.replace(p, " "+lat)
    text = text.replace("TAU", "tau").replace(
        "MU", "mu").replace("NU", "nu").replace("GAMMA", "gamma")
    while "  " in text:
        text = text.replace("  ", " ").strip()
    tokens = []
    for token in text.split(","):
        token=token.strip()
        if "BAR" in token:
            tokens += ["\\bar{%s}" % token.replace("BAR", "")]
        elif "~" in token:
            tokens += ["\\bar{%s}" % token.replace("~", "")]
        else:
            tokens += [token]
    text = ",".join(tokens)
    text = text.replace(">", " \\rightarrow ")
    if not args._splitIncomingPartons:
        text = mergeIncomingPartons(text)
    if not args._splitLightQuarks:
        text = groupLightQuarks(text, args._splitBQuarks)
    if not args._splitAntiParticles:
        text = ignoreAntiParticles(text)
    if args._mergeEWBosons:
        text = mergeEWBosons(text)
    if not args._splitLeptons:
        text = mergeLeptons(text)
    return text

def removeNeutrinoFlavours(text):
    tokens = [x if not "nu" in x else "nu" for x in text.split(",")]
    text = ", ".join(tokens)
    return text

def mergeIncomingPartons(text):
    sep = " \\rightarrow "
    partons=  text.split(sep)[0]
    products= text.split(sep)[-1]
    text = text.replace("bar", "BAR")
    partons = groupLightQuarks(ignoreAntiParticles(partons))
    partons = partons.replace("q", "p").replace("g", "p")
    text = text.replace("BAR", "bar")
    text = partons+sep+products
    return text


def mergeEWBosons(text, mergeHiggs=True):
    if mergeHiggs:
        text = text.replace("H", "V")
    text = text.replace("Z0", "V")
    text = text.replace("W+", "V")
    text = text.replace("W-", "V")
    text = text.replace("Z", "V")
    text = text.replace("W", "V")
    return text


def mergeLeptons(text):
    text = text.replace("e", "l")
    text = text.replace("\\mu", "l")
    text = text.replace("\\tau", "l")
    return text


def ignoreAntiParticles(text):
    text = text.replace("~", "")

    # remove charge
    tokens = []
    for token in text.split(","):
        is_rec_particle = False
        for key, value in recognisedParticles.items():
            if key in token:
                is_rec_particle = True
                break
        
        if is_rec_particle:
            token = token.replace("+", "")
            token = token.replace("-", "")
        else:
            token = token.replace("+", "PLUSORMINUS")
            token = token.replace("-", "PLUSORMINUS")
            token = token.replace("PLUSORMINUS", "_ch")
        tokens.append(token.strip())
    text = ",".join(tokens)

    # remove \bar{}
    tokens = []
    for token in text.split(","):
        if "bar" in token or "BAR" in token:
            token = token.split("{")[-1].split("}")[0]
        tokens += [token.strip()]
    res=", ".join(tokens)
    return res


def groupLightQuarks(text, splitBquarks=False, splitTquarks=False):
    tokens = []
    for t in text.split(","):
        t=t.strip()
        
        qStrings = [
                    "u", "s", "d", "c" ,"b", "t", 
                     "ubar", "dbar", "cbar", "sbar", "bbar", "tbar",
                     "\\bar{u}", "\\bar{d}", "\\bar{c}", "\\bar{s}", "\\bar{b}", "\\bar{t}" 
                   ] 
        if t in  qStrings:

            t = t.replace("ubar", "qbar")
            t = t.replace("dbar", "qbar")
            t = t.replace("sbar", "qbar")
            t = t.replace("cbar", "qbar")
            t = t.replace("u", "q")
            t = t.replace("s", "q")
            t = t.replace("d", "q")
            t = t.replace("c", "q")
            if not (splitBquarks):
                t = t.replace("bar", "BAR").replace(
                    "b", "q").replace("BAR", "bar")
                t = t.replace("bbar", "qbar")
            if not (splitTquarks):
                t = t.replace("t", "q")
                t = t.replace("tbar", "qbar")
        tokens += [t]
    text = ", ".join(tokens)
    return text


def getBRs(fbr): # to decode the branch fraction blocks in the Herwig logs
    BRDict = {}
    inBRBlock = False
    for line in fbr.readlines():
        line = line.strip()
        if "# Parent:" in line:
            inBRBlock = True
            continue
        if inBRBlock and "#" in line:
            continue
        if inBRBlock and not "->" in line:
            inBRBlock = False
            continue
        if not inBRBlock:
            continue
        p_d_string = makeLatexSafe(line.split(
            ";")[0], debug=1).split("\\rightarrow")
        parent = p_d_string [0]
        decay = p_d_string [1]
        br = float(line.split()[2])
        if br == 0:
            continue
        parent = parent.strip()
        if not parent in BRDict.keys():
            BRDict[parent] = {}
        if not decay in BRDict[parent].keys():
            BRDict[parent][decay] = 0
        BRDict[parent][decay] += br

    # add SM BRs:
    if not args._splitByBSMBR:
        if not "Z" in BRDict.keys():
            BRDict["Z"] = {}
            BRDict["Z"]["\\nu, \\nu"] = 0.205
            if args._splitLeptons:
                BRDict["Z"]["e, e"] = 0.035
                BRDict["Z"]["\\mu, \\mu"] = 0.035
                BRDict["Z"]["\\tau, \\tau"] = 0.035
            else:
                BRDict["Z"]["l, l"] = 0.105
            if args._splitBQuarks:
                BRDict["Z"]["b, b"] = 0.15
                BRDict["Z"]["q, q"] = 0.54
            else:
                BRDict["Z"]["q, q"] = 0.69

        if not "W" in BRDict.keys():
            BRDict["W"] = {}
            if args._splitLeptons:
                BRDict["W"]["e, \\nu"] = 0.105
                BRDict["W"]["\\mu, \\nu"] = 0.105
                BRDict["W"]["\\tau, \\nu"] = 0.105
            else:
                BRDict["W"]["l, \\nu"] = 0.32
            BRDict["W"]["q, q"] = 0.68

        if not "t" in BRDict.keys():
            if not args._splitBQuarks:
                BRDict["t"] = {"W, q": 1.0}
            else:
                BRDict["t"] = {"W, b": 1.0}
    else:
        #del BRDict["H"]
        if "H" in BRDict.keys(): del BRDict["H"]
    return BRDict


def printBRs(BRDict, tol=0.01):
    for proc, decays in BRDict.items():
        print("Decays of %s" % proc)
        countMinorDecays = 0
        for decay, br in sorted(decays.items(), key=lambda x: x[1], reverse=True):
            if br > tol:
                print("--> %s = %.2f%%" % (decay, br*100))
            else:
                countMinorDecays += 1
        if countMinorDecays:
            print("(skipped %d decays with BR<%.1f%%)" %
                  (countMinorDecays, tol*100))


def validateLine(txt):
    tokens = txt.split()
    if not len(tokens) == 4:
        return False
    if not removeBrackets(tokens[1]) >= 0:
        return False
    if not removeBrackets(tokens[2]) >= 0:
        return False
    if not removeBrackets(tokens[3]) >= 0:
        return False
    return True


def removeBrackets(txt):
    res = txt.split("(")[0]+txt.split(")")[-1]
    try:
        res = float(res)
    except:
        res = -1.

    return float(res)

# format input dir path
if args.inputDir[-1] == "/":
    args.inputDir = args.inputDir[:-1]

# open the params file and print values
number = args.inputDir.split("/")[-1]
param_file = args.inputDir+"/params.dat"
if not os.path.isfile(param_file):
    print("[ERROR] Required file %s does not exist. Abort." % param_file)
    exit()
fParams = open(param_file)
vals={}
valsText=""
for l in fParams.readlines():
    l = l.strip()
    tokens = l.split(" = ")
    vals[tokens[0]] = float(tokens[1])
    valsText+="%s=%f, "%(tokens[0],float(tokens[1]))
valsText=valsText[:-2]
print(" %s :: %s " % (
    args.inputDir, valsText))

#### ANALYZE CROSS SECTION AND BRANCHING FRACTIONS  ####
fxs = open("%s/LHC-S101-runpoint_%s.out" % (args.inputDir, number))
fbr = open("%s/LHC-S101-runpoint_%s.log" % (args.inputDir, number))
brs = getBRs(fbr)
#printBRs(brs, args._tolerance)
totalXS = -999.
runningTotXS = 0.
procDict = {}
fxs_lines=fxs.readlines()

#populate herwigMEChainMap
for l in fxs_lines:
    l = l.strip()
    if not "(PPExtractor)" in l:
        continue
    # example (PPExtractor) ubar u (MEuubar2XmXm u,ubar->Xm,Xm)
    # we want the part in the second brackets
    tokenOfInterest=l.split("(")[-1].split(")")[0]
    key=tokenOfInterest.split(" ")[0]
    value=tokenOfInterest.split(" ")[-1]
    if key in herwigMEChainMap.keys():
      assert(herwigMEChainMap[key]==value)
    herwigMEChainMap[key]=value

# extract raw cross-sections for processes which contribute at this point
for l in fxs_lines:
    l = l.strip()
    if "PPExtractor" in l:
        continue
    if "Total (from generated events)" in l:
        l = l.replace("Total (from generated events)", "")
        totalXS = removeBrackets(l.split()[3])
        if not args._ws: 
            print("totalXS %.2f fb"% (totalXS *1e6))
    tokens = l.split()
    if validateLine(l):
        procName = tokens[0]
        thisXS = removeBrackets(tokens[3])
        if thisXS == 0:
            continue
        if ":" in procName:
            continue
        runningTotXS += thisXS
        procName=herwigMEChainMap[procName]
        procName = makeLatexSafe(procName, isME=1)
        if args._splitByBR:
            pass
            # printBRs(brs)
            
            # if requested, recursively apply branching fractions for SM and BSM particles
            # until only stable particles remain in the reaction
            def applyBRs(text, brs, procDict, validationDict, totBR=1, indent=""):
                if len(indent) > 100:
                    if not text in procDict.keys():
                        procDict[text] = 0
                    procDict[text] += 100*totBR
                    if not text in validationDict.keys():
                        validationDict[text] = 0
                    validationDict[text] += 100*totBR
                    return

                hasUnstableParticles = False
                baseText = text.replace("\\rightarrow", "-->")
                tokenCounter = -1
                tokenBreak = 0
                for token in baseText.split(","):
                    tokenCounter += 1
                    if tokenBreak:
                        break
                    if token in brs.keys():
                        hasUnstableParticles = True
                        for decay, br in brs[token].items():
                            newTextTokens = baseText.split()
                            newTextTokens[tokenCounter] = decay
                            newText = ", ".join(newTextTokens).strip()
                            newTokens = []
                            for token in newText.split(" --> "):
                                newTokens += [", ".join(sorted(token.split(","))).strip()]
                            text = " --> ".join(newTokens)
                            # print indent+"%s, BR=%.4f, totBR=%.4f"%(newText, 100*br, 100*totBR*br)

                            applyBRs(text, brs, procDict,
                                     validationDict,  totBR*br, indent+"+")
                        tokenBreak = 1

                if hasUnstableParticles == False:
                    # print "===> text ''%s'' is stable! BR= %.4f"%(text, 100*totBR)
                    if not text in procDict.keys():
                        procDict[text] = 0
                    procDict[text] += 100*totBR
                    if not text in validationDict.keys():
                        validationDict[text] = 0
                    validationDict[text] += 100*totBR

            validationDict = {}
            applyBRs(procName, brs, procDict, validationDict, thisXS/totalXS)
            sumBR = 0
            for k, v in sorted(validationDict.items(), key=lambda x: x[0]):
                sumBR += v*(totalXS/thisXS)
            assert(abs(sumBR-100) < 0.1)

        else:
            if not procName in procDict.keys():
                procDict[procName] = 0
            procDict[procName] += 100*thisXS/totalXS


for k, v in reversed(sorted(procDict.items(), key=lambda item: item[1])):
    k=cleanupCommas(k)
    if v < 100*float(args._tolerance):
        break

    if not args._ws:  
        print("%.2f fb, (%.2f%%), %s"%(v*totalXS*0.01*1e6,v, k.replace("-->","\\rightarrow")))
    else:
        print("%s [%.2f fb, %.2f%%]"%(k.replace("\\rightarrow","->"), v*totalXS*0.01*1e6,v))

