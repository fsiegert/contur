#!/usr/bin/env python

import os
import pickle
import sys
from argparse import ArgumentParser

parser = ArgumentParser(description="Concatenate multiple .map files.")
parser.add_argument('map_files', nargs='*', metavar='map_files',
                    help='Specify .map files to concatenate.')
parser.add_argument('-o', '--output_path', metavar='out_path', type=str,
                    default='merged.map',
                    help='Output path to write new .map file to.')
args = parser.parse_args()

if os.path.isfile(args.output_path):
    permission = ''
    while permission.lower() not in ['n', 'no', 'y', 'yes']:
        permission = input("Output file '%s' already exists. Do you want "
                               "to overwrite it?\n[y/N] :" % args.output_path)
    if permission.lower() in ['n', 'no']:
        sys.exit()

combined_object = []
target = None
for map_file in args.map_files:
    with open(map_file, 'rb') as f:
        print("merging ", f)
        if not target:
            target = pickle.load(f)
            continue
        else:
            candidate = pickle.load(f)
            target.mergeMaps(candidate)

if target:
    target.resortPoints()

with open(args.output_path, 'wb') as f:
    pickle.dump(target, f, protocol=2)
