read FRModel.model 

# Parameters specified in param_file.dat
## See also https://feynrules.irmp.ucl.ac.be/wiki/VLQ

## Nominal masses of the T, X, B and Y VLQs
set /Herwig/FRModel/Particles/tp:NominalMass {mtp} 
set /Herwig/FRModel/Particles/x:NominalMass {mx}
set /Herwig/FRModel/Particles/bp:NominalMass {mbp}
set /Herwig/FRModel/Particles/y:NominalMass {my}

## VB Branching fractions of the VLQs, ie T,B->H, T,B->Z, T,B->W 
set /Herwig/FRModel/FRModel:xitph {xitph} 
set /Herwig/FRModel/FRModel:xitpz {xitpz} 
set /Herwig/FRModel/FRModel:xitpw {xitpw}
set /Herwig/FRModel/FRModel:xibpw {xibpw}
set /Herwig/FRModel/FRModel:xibpz {xibpz}
set /Herwig/FRModel/FRModel:xibph {xibph}

## Coupling strength of the VLQs. Affects single production, and the total lifetime.
set /Herwig/FRModel/FRModel:KX {kappa}
set /Herwig/FRModel/FRModel:KY {kappa}
set /Herwig/FRModel/FRModel:KT {kappa}
set /Herwig/FRModel/FRModel:KB {kappa}

## These set which generation quarks are produced in the VB decays.
## via the convention zeta[VLQ][quark flavour]L 
## Most studies set the 1st and 2nd gen to zero (zetaXuL,zetaTcL, etc...)
set /Herwig/FRModel/FRModel:zetaXuL {zetaXuL}
set /Herwig/FRModel/FRModel:zetaXcL {zetaXcL} 
set /Herwig/FRModel/FRModel:zetaXtL {zetaXtL} 
set /Herwig/FRModel/FRModel:zetaTuL {zetaTuL} 
set /Herwig/FRModel/FRModel:zetaTcL {zetaTcL} 
set /Herwig/FRModel/FRModel:zetaTtL {zetaTtL} 
set /Herwig/FRModel/FRModel:zetaBdL {zetaBdL} 
set /Herwig/FRModel/FRModel:zetaBsL {zetaBsL} 
set /Herwig/FRModel/FRModel:zetaBbL {zetaBbL} 
set /Herwig/FRModel/FRModel:zetaYdL {zetaYdL} 
set /Herwig/FRModel/FRModel:zetaYsL {zetaYsL} 
set /Herwig/FRModel/FRModel:zetaYbL {zetaYbL} 


####################################
#
# Modify the required process here
#
####################################


cd /Herwig/NewPhysics

insert HPConstructor:Incoming 0 /Herwig/Particles/u
insert HPConstructor:Incoming 1 /Herwig/Particles/ubar
insert HPConstructor:Incoming 0 /Herwig/Particles/d
insert HPConstructor:Incoming 0 /Herwig/Particles/dbar
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 0 /Herwig/Particles/s
insert HPConstructor:Incoming 0 /Herwig/Particles/sbar
insert HPConstructor:Incoming 0 /Herwig/Particles/b
insert HPConstructor:Incoming 0 /Herwig/Particles/bbar
insert HPConstructor:Incoming 0 /Herwig/Particles/c
insert HPConstructor:Incoming 0 /Herwig/Particles/cbar

insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/x
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/x~
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/tp
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/tp~
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/bp
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/bp~
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/y
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/y~

set HPConstructor:Processes SingleParticleInclusive

#############################################################
## Additionally, you can use new particles as intermediates
## with the ResConstructor:
#############################################################
insert ResConstructor:Incoming 0 /Herwig/Particles/u
insert ResConstructor:Incoming 0 /Herwig/Particles/ubar
insert ResConstructor:Incoming 0 /Herwig/Particles/d
insert ResConstructor:Incoming 0 /Herwig/Particles/dbar
insert ResConstructor:Incoming 0 /Herwig/Particles/s
insert ResConstructor:Incoming 0 /Herwig/Particles/sbar
insert ResConstructor:Incoming 0 /Herwig/Particles/c
insert ResConstructor:Incoming 0 /Herwig/Particles/cbar
insert ResConstructor:Incoming 0 /Herwig/Particles/b
insert ResConstructor:Incoming 0 /Herwig/Particles/bbar
insert ResConstructor:Incoming 0 /Herwig/Particles/g
#
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/x
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/x~
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/y
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/y~
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/bp
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/bp~
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/tp
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/tp~
#
insert ResConstructor:Outgoing 0 /Herwig/Particles/e+
insert ResConstructor:Outgoing 1 /Herwig/Particles/W+
insert ResConstructor:Outgoing 1 /Herwig/Particles/W-
insert ResConstructor:Outgoing 2 /Herwig/Particles/Z0
insert ResConstructor:Outgoing 2 /Herwig/Particles/h0
insert ResConstructor:Outgoing 3 /Herwig/Particles/gamma
insert ResConstructor:Outgoing 3 /Herwig/Particles/g
insert ResConstructor:Outgoing 3 /Herwig/Particles/u
insert ResConstructor:Outgoing 3 /Herwig/Particles/ubar
insert ResConstructor:Outgoing 3 /Herwig/Particles/d
insert ResConstructor:Outgoing 3 /Herwig/Particles/dbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/s
insert ResConstructor:Outgoing 3 /Herwig/Particles/sbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/b
insert ResConstructor:Outgoing 3 /Herwig/Particles/bbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/t
insert ResConstructor:Outgoing 3 /Herwig/Particles/tbar



###########################################################
# Specialized 2->3 higgs constructors are also available,
# where incoming lines don't need to be set.
###########################################################
## Higgs + t tbar
# set /Herwig/NewPhysics/QQHiggsConstructor:QuarkType Top
# insert /Herwig/NewPhysics/QQHiggsConstructor:HiggsBoson  0 [HIGGS_NAME]
#
## Higgs VBF
# insert /Herwig/NewPhysics/HiggsVBFConstructor:HiggsBoson  0 [HIGGS_NAME]
#
## Higgs + W/Z, with full 2->3 ME
# set /Herwig/NewPhysics/HVConstructor:CollisionType Hadron
# insert /Herwig/NewPhysics/HVConstructor:VectorBoson 0 /Herwig/Particles/Z0
# insert /Herwig/NewPhysics/HVConstructor:HiggsBoson  0 [HIGGS_NAME]



####################################
####################################
####################################
read snippets/PPCollider.in

# Intrinsic pT tune extrapolated to LHC energy
set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV

# disable default cuts if required
# cd /Herwig/EventHandlers
# create ThePEG::Cuts   /Herwig/Cuts/NoCuts
# set EventHandler:Cuts /Herwig/Cuts/NoCuts

# Other parameters for run
cd /Herwig/Generators
set EventGenerator:NumberOfEvents 10000000
set EventGenerator:RandomNumberGenerator:Seed 31122001
set EventGenerator:DebugLevel 0
set EventGenerator:EventHandler:StatLevel Full
set EventGenerator:PrintEvent 100
set EventGenerator:MaxErrors 10000
