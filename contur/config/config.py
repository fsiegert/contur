"""Global configuration class setting default behavior, importing contur loads all of the following

:Module members:
    * **buildCorr** (``bool``) --
      Global bool flag controlling whether to build and use correlations or not (*Default=* ``True``)
    * **useTheory** (``bool``) --
      Global bool flag controlling whether to use Theory predictions for background models where found (*Default=* ``False``)
    * **useTheoryUncert** (``bool``) --
      Global bool flag controlling whether to try and use theoretical uncertainty (*Default=* ``False``)
    * **vetoAnalyses** (``string``) --
      List of strings to veto from analyses loaded (*Default=* ``[]``)
    * **onlyAnalyses** (``string``) --
      List of strings to only load analyses containing said strings (*Default=* ``[]``)
    * **gridMode** (``bool``) --
      Global bool flag dictating if running on a grid (*Default=* ``False``)
        Essentially disables a lot of plotting options to speedup

    * **excludeMETRatio** (``bool``) --
      Global bool flag controlling whether to use the ATLAS MET Ratio measurement (*Default=* ``False``)
    * **excludeHgg** (``bool``) --
      Global bool flag controlling whether to use Higgs to diphoton measurements (*Default=* ``False``)
    * **excludeHWW** (``bool``) --
      Global bool flag controlling whether to exclude Higgs to WW measurements (*Default=* ``True``)
    * **excludeAWZ** (``bool``) --
      Global bool flag controlling whether to exclide ATLAS WZ SM measurement (*Default=* ``True``)
"""

#Setup some module level variable defaults

buildCorr=False
useTheory=False
theoryOnly=False
useTheoryCorr=True
expectedLimit=False
vetoAnalyses=[]
onlyAnalyses=[]
splitAnalysis=False
gridMode=False
min_syst=0.001
weight=""
refLoaded=False

# minimum number of systematic uncertainty contributions before they will be treated as correlated
min_num_sys=2
# termination criteria for scipy.minimize
ll_prec=0.01
err_prec=0.01
n_iter=10

excludeMETRatio=False
excludeHgg=False
excludeHWW=True
excludeBVeto=True
excludeAWZ=True
excludeSearches=True

binwidth=-100.
binoffset=0.

using_condor=False
using_qsub  =True
using_slurm =False

mceg="herwig"
mceg_template="LHC.in"

version="2.0.1"

# Some default filenames.
paramfile="params.dat"
tag="runpoint_"
unneeded_files=["LHC.run","Looptools.log", "py.py", "mgevents"]
blocklist={"MASS","STOPMIX"}

import re
ANALYSISPATTERN = re.compile(r'([A-Z0-9]+_\d{4}_[IS]\d{5,8})')
ANALYSIS        = re.compile(r'([A-Z0-9]+_\d{4}_[IS]\d{5,8}[^/]*)')
#ANALYSISHISTO   = re.compile('[A-Z0-9]+_\d{4}_[IS]\d{4,8}[^\/]\S*')
ANALYSISHISTO   = re.compile(r'([A-Z0-9]+_\d{4}_[IS]\d{5,8}[^/]*)/((d\d+-x\d+-y\d+)|\w+)')

