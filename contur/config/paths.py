def root_path():
    "Get the Contur installation's root path"
    import os
    croot = os.environ.get("CONTUR_ROOT", os.environ.get("CONTURMODULEDIR"))
    if croot is None:
        raise Exception("CONTUR_ROOT not defined")
    return croot

def path(*relpaths):
    "Get the complete path to a sub-path in the Contur installation"
    import os.path
    return os.path.join(root_path(), *relpaths)
