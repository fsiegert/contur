"""
The Depot module contains the Depot class defining the parent analysis class

This is intended to be the high level analysis control, most user access methods should be implemented at this level
"""

# import cPickle as pickle
import os
import pickle
from collections import defaultdict

import contur
import numpy as np
from contur.factories.yoda_factories import YodaFactory


class Depot(object):
    """ Parent analysis class to initialise

    This can be initialised as a blank canvas, then the desired workflow is to add parameter space points to the Depot using
    the :func:`add_point` method. This appends each considered point to the objects internal :attr:`inbox`

    :Keyword Arguments:
        * **outputDir** (``string``) -- Path for writing out objects
        * **noStack** (``Bool``) -- Flag if visual objects should be automatically stacked

    .. todo:: **kwargs no longer necessary, check if can be safely removed
    """

    def __init__(self, outputDir="plots", noStack=False):

        self._inbox = []
        self._bookmapAxis = defaultdict(list)
        self.meshGrid = None
        self.gridPoints = None

        # map axis and plot axis are two different objects due to idiosyncracies in pcolormesh
        # mapAxis is the true sampled points
        # plotAxis is the offset values for plotting
        # _bookmapAxis is just for bookkeeping
        self._bookmapAxis = defaultdict(list)
        self._mapAxis = {}
        self._plotAxis = {}

        self._OutputDir = outputDir
        self._NoStack = noStack

    def build_axes(self):
        """Function to build the axis dictionaries used for plotting, parameter space points are otherwise stored unordered

        :Built variables:
            * **mapAxis** (``dict``)
            * **plotAxis** (``dict``)

        """
        for i in self._inbox:
            for k, v in i.param_point.items():
                self._bookmapAxis[k].append(v)

        # Pcolormesh (which we use for visualisation) centres grid in bottom left corner of a cell, we need to shift the axis
        # this is a pain but we will truncate at the original max/min anyway so a guess is fine
        # first build the true axes
        for k, v in self._bookmapAxis.items():
            if k != "slha_file":
                self._mapAxis[k] = np.unique(np.array(v, dtype=float))
                self._plotAxis[k] = (
                    self._mapAxis[k][1:] + self._mapAxis[k][:-1]) / 2
                # now the ugly offset
                if self._plotAxis[k].any():
                    try:
                        self._plotAxis[k] = np.insert(self._plotAxis[k], 0, self._plotAxis[k][0] - self._plotAxis[k][
                            1])  # -self.mapAxis[k][0])
                        self._plotAxis[k] = np.append(self._plotAxis[k], self._mapAxis[k][-1] + (
                            self._mapAxis[k][-1] - self._plotAxis[k][-1]))
                    except:
                        self._plotAxis[k] = self._mapAxis[k]
                else:
                    self._plotAxis[k] = self._mapAxis[k]

    def write(self, outDir):
        """Function to write out a "map" file containing the full pickle of this depot instance

        :param outDir:
            String of filesystem location to write out the pickle of this instance to
        :type outDir: ``string``

        """
        contur.util.mkoutdir(outDir)
        path_out = os.path.join(outDir, 'contur.map')

        contur.config.contur_log.info("Writing output map to : " + path_out)

        with open(path_out, 'wb') as f:
            pickle.dump(self, f, protocol=2)

    def add_custom_point(self, logfile, param_dict):
        """Function to add a custom file, typically read as a string, to the """
        self._inbox.append(ParamYodaPoint(
            yodaFactory=logfile, paramPoint=param_dict))

    def add_point(self, yodafile, param_dict):
        """Add yoda file and the corresponding parameter point into the depot"""
        yFact = YodaFactory(yodaFilePath=yodafile,
                            outdir=self._OutputDir, noStack=self._NoStack)
        yFact.sort_blocks()
        yFact.build_full_likelihood()
        contur.config.contur_log.info(
            "Added yodafile with reported exclusion of: " + str(yFact.likelihood.CLs))
        self._inbox.append(ParamYodaPoint(
            yodaFactory=yFact, paramPoint=param_dict))

    def resort_points(self):
        """Function to trigger rerunning of the sorting algorithm on all items in the inbox, typically if this list has been affected by a merge by a call to :func:`contur.depot.mergeMaps`
        """
        # convenience function to re-run the sorting algorithm on all points in the depot incase of modification of underlying tests
        for p in self._inbox:
            # p.yoda_factory._testMethod=self._testMethod
            p.yoda_factory._resort_blocks()
            # del p.paramHolder.conturPoints

    def merge(self, depot):
        """Function to merge one conturDepot instance with another, note matching of parameter point for each item in the target instance is required or the entry will be skipped

        :param depot:
            Additional instance to conturDepot to merge with this one
        :type depot: :class:`contur.conturDepot`

        """
        for point in depot.inbox:
            for p in self._inbox:
                same = True
                for key, value in p.param_point.items():
                    try:
                        if point.param_point[key] != value and not key.startswith("AUX:"):
                            same = False
                    except:
                        contur.config.contur_log.warning(
                            "Not merging. Key not found:" + key)
                        same = False
                if same:
                    p.yoda_factory.sorted_likelihood_blocks.extend(
                        point.yoda_factory.sorted_likelihood_blocks)

                    #[p.yoda_factory.sortedBuckets.extend(point.yoda_factory.sortedBuckets) for p in self.conturDepotInbox if p.param_point == point.param_point]

    def _build_frame(self, include_dominant_pools=False):
        """:return pandas.DataFrame of the inbox points"""
        try:
            import pandas as pd
        except ImportError:
            contur.config.contur_log.error("Pandas module not available. Please, ensure it is installed and available in your PYTHONPATH.")

        # Use the first point in the inbox to make a frame
        try:
            frame = pd.DataFrame(
                [param_yoda_point.param_point for param_yoda_point in self._inbox])
            frame['CL'] = [
                param_yoda_point.yoda_factory.likelihood.CLs for param_yoda_point in self._inbox]
            if include_dominant_pools:
                frame['dominant-pool'] = [
                    param_yoda_point.yoda_factory.dominant_pool.pools 
                    for param_yoda_point in self._inbox
                ]
                frame['dominant-pool-tag'] = [
                    param_yoda_point.yoda_factory.dominant_pool.tags 
                    for param_yoda_point in self._inbox
                ]
            return frame
        except:
            contur.config.contur_log.error(
                "Inbox is empty, add parameter points to depot")

    def export(self, path, include_dominant_pools=True):
        self._build_frame(include_dominant_pools).to_csv(path)

    @property
    def inbox(self):
        """The master list of :class:`~contur.factories.depot.ParamYodaPoint` instances added to the Depot instance

        **type** ( ``list`` [ :class:`~contur.factories.depot.ParamYodaPoint` ])
        """
        return self._inbox

    @property
    def frame(self):
        """A ``pandas.DataFrame`` representing the CLs interval for each point in :attr:`inbox`

        **type** (``pandas.DataFrame``)
        """
        return self._build_frame()

    @property
    def map_axis(self):
        """Dictionary of the sampled values in each axis

        **type** (``dict``) --
        **key** -- Parameter name (``string``),
        **value** -- (:class:`numpy.ndarray`)
        """
        return self._mapAxis

    @map_axis.setter
    def map_axis(self, value):
        self._mapAxis = value

    @property
    def plot_axis(self):
        """Dictionary of the offset midpoints in each axis, for colormesh purposes

        **type** (``dict``) --
        **key** -- Parameter name (``string``),
        **value** -- (:class:`numpy.ndarray`)
        """
        return self._plotAxis

    @plot_axis.setter
    def plot_axis(self, value):
        self._plotAxis = value

    def __repr__(self):
        return "%s with %s added points" % (self.__class__.__name__, len(self._inbox))


class ParamYodaPoint(object):
    """Book keeping class relating

    :param paramPoint:
        **key** ``string`` param name : **value** ``float``
    :type paramPoint: ``dict``
    :param yodaFactory:
        yoda_factory object, conturs YODA file reader
    :type yodaFactory: :class:`contur.depot.yoda_factory`

    """

    def __init__(self, paramPoint, yodaFactory):
        self.param_point = paramPoint
        self.yoda_factory = yodaFactory

    def __repr__(self):
        return repr(self.param_point)
