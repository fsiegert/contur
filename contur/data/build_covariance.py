import numpy as np
import contur

class CovarianceBuilder(object):
    """
    `ao` Yoda AO

    Class to handle retrieval of annotations/errors from YODA objects
    """
    def __init__(self, ao):
        self.ao=ao
        self.hasBreakdown=self._getCorAttr()
        self.nbins=self.ao.numPoints()
        self.cov=None
        self.uncov=None
        self.errorBreakdown={}

    def _getCorAttr(self):
        if not contur.config.buildCorr:
            return False
        if not self.ao.hasValidErrorBreakdown():
            return False
        if len(self.ao.variations())<contur.config.min_num_sys:
            return False
        return True

    def buildCovFromBreakdown(self,ignore_corrs=False):
        """Build the covariance from error breakdown"""

        # now store the contributions to the correlated systematic uncertainties
        for source in self.ao.variations():
            if len(source)>0 and not "stat" in source and not "Uncor" in source and not "uncor" in source:
                systErrs = np.zeros(self.nbins)
                fracErrs = np.zeros(self.nbins)
                ibin=0
                for point in self.ao.points():
                    nomVal = point.y()
                    try:
                        #symmetrize the errors (be conservative - use the largest!)
                        systErrs[ibin]=max(abs(point.errMap()[source][0]),abs(point.errMap()[source][1]))
                        fracErrs[ibin] = systErrs[ibin]/nomVal
                    except:
                        # handle occasional messed up entries
                        systErrs[ibin]=0
                    ibin=ibin+1
                if max(fracErrs)> contur.config.min_syst:
                    self.errorBreakdown[source] = systErrs

        return self.ao.covarianceMatrix(ignore_corrs) 

    def buildCovFromErrorBar(self,assume_correlated=False):
        """Build the covariance from error bars"""
        dummyM = np.outer(range(self.nbins), range(self.nbins))
        covM = np.zeros(dummyM.shape)
        systErrs = np.zeros(self.nbins)
        for ibin in range(self.nbins):
            #symmetrize the errors (be conservative - use the largest!)
             #systErrs[ibin] = ((abs(self.ao.points()[ibin].yErrs()[0])) + (abs(self.ao.points()[ibin].yErrs()[1]))) * 0.5
            systErrs[ibin] = max(abs(self.ao.points()[ibin].yErrs()[0]),abs(self.ao.points()[ibin].yErrs()[1]))
        # Special case: if the errors are all zeros, this is a search with event counts, so set errors equal to the sqrt of the
        # value, or one, which ever is largest (NO LONGER NEEDED. HANDLE IN REF INIT)
        #if not systErrs.any():
        #    for ibin in range(self.nbins):
        #        systErrs[ibin] = max(np.sqrt(self.ao.points()[ibin].y()),1.0)
            
        if assume_correlated:
            self.errorBreakdown['syst']=systErrs
            covM += np.outer(systErrs, systErrs)
            # Store the systematic uncertainty for each bin and contribution.
        else:
            covM += np.diag(systErrs * systErrs)

        return covM

    def getErrorBreakdown(self):
        return self.errorBreakdown
            


